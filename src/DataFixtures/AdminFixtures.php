<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

// create an example of admin in database in order to login in Back office
class AdminFixtures extends Fixture
{
    private $email = "admin@admin.com";
    private $firstname = "fn";
    private $lastname = "ln";
    private $role = ["ROLE_ADMIN"];
    private $password = '$2y$13$Bnw0JzghWiF8hBgcgOxvouSIkAXy7H2dQi12LXRCzqw1VJGhX1QKu'; // the password will be "password"

    public function load(ObjectManager $manager): void
    {
        $user = new User();
        $user->setEmail($this->email);
        $user->setRoles($this->role);
        $user->setFirstname($this->firstname);
        $user->setLastname($this->lastname);
        $user->setPassword($this->password);
        $manager->persist($user);

        $manager->flush();
    }
}
