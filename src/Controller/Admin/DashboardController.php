<?php

namespace App\Controller\Admin;

use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Entity\Tag;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator
    ) {
    }
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        $url = $this->adminUrlGenerator
            ->setController(ProductCrudController::class)
            ->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Admin dashboard')
            ->generateRelativeUrls();
    }

    public function configureMenuItems(): iterable
    {

        yield MenuItem::linkToDashboard('Home', 'fa fa-home');

        yield MenuItem::section("Sections");
        yield MenuItem::subMenu('Products', 'fa-solid fa-dolly')
            ->setSubItems([
                MenuItem::linkToCrud('Show products', 'fa-regular fa-eye', Product::class)
                    ->setDefaultSort(['createdAt' => 'DESC']),
                MenuItem::linkToCrud('Add product', 'fa-solid fa-cart-plus', Product::class)
                    ->setAction("new"),
                    // MenuItem::linkToLogout('Logout', 'fa fa-exit'),
            ]);

        yield MenuItem::subMenu('Tags', 'fa-solid fa-tags')
            ->setSubItems([
                MenuItem::linkToCrud('Show tags', 'fa-regular fa-eye', Tag::class)
                    ->setDefaultSort(['createdAt' => 'DESC']),
                MenuItem::linkToCrud('Add tag', 'fa-solid fa-tag', Tag::class)
                    ->setAction("new")
            ]);
    }
}
